/*
Navicat MySQL Data Transfer

Source Server         : java
Source Server Version : 50718
Source Host           : cdb-klkyv4dq.bj.tencentcdb.com:10182
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2020-02-26 06:22:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_advert
-- ----------------------------
DROP TABLE IF EXISTS `t_advert`;
CREATE TABLE `t_advert` (
  `advert_id` int(11) NOT NULL AUTO_INCREMENT,
  `advert_image` varchar(32) NOT NULL,
  `advert_link` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`advert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_appt_trans
-- ----------------------------
DROP TABLE IF EXISTS `t_appt_trans`;
CREATE TABLE `t_appt_trans` (
  `at_id` int(11) NOT NULL AUTO_INCREMENT,
  `at_user` varchar(255) DEFAULT NULL,
  `at_hospital` varchar(255) DEFAULT NULL,
  `at_vaccine` varchar(255) DEFAULT NULL,
  `at_appt_date` datetime DEFAULT NULL,
  `at_record_date` datetime DEFAULT NULL,
  PRIMARY KEY (`at_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_area
-- ----------------------------
DROP TABLE IF EXISTS `t_area`;
CREATE TABLE `t_area` (
  `aid` int(11) NOT NULL,
  `area_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_hospital
-- ----------------------------
DROP TABLE IF EXISTS `t_hospital`;
CREATE TABLE `t_hospital` (
  `hospital_id` int(11) NOT NULL AUTO_INCREMENT,
  `hospital_name` varchar(32) DEFAULT NULL,
  `hospital_site` varchar(32) DEFAULT NULL,
  `hospital_area` int(5) DEFAULT NULL,
  PRIMARY KEY (`hospital_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_notice
-- ----------------------------
DROP TABLE IF EXISTS `t_notice`;
CREATE TABLE `t_notice` (
  `notice_id` int(11) NOT NULL AUTO_INCREMENT,
  `notice_title` varchar(32) NOT NULL,
  `notice_content` text,
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_permission`;
CREATE TABLE `t_permission` (
  `permissionid` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`permissionid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_question
-- ----------------------------
DROP TABLE IF EXISTS `t_question`;
CREATE TABLE `t_question` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_name` varchar(255) NOT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_questionnaire
-- ----------------------------
DROP TABLE IF EXISTS `t_questionnaire`;
CREATE TABLE `t_questionnaire` (
  `questionnaire_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_cid` int(11) NOT NULL,
  `question_answer` varchar(255) DEFAULT NULL,
  `user_cid` int(11) DEFAULT NULL,
  PRIMARY KEY (`questionnaire_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT,
  `rolename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`roleid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_role_permission`;
CREATE TABLE `t_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionid` int(11) DEFAULT NULL,
  `roleid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kf_3` (`permissionid`),
  KEY `kf4_4` (`roleid`),
  CONSTRAINT `kf4_4` FOREIGN KEY (`roleid`) REFERENCES `t_role` (`roleid`),
  CONSTRAINT `kf_3` FOREIGN KEY (`permissionid`) REFERENCES `t_permission` (`permissionid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_tips
-- ----------------------------
DROP TABLE IF EXISTS `t_tips`;
CREATE TABLE `t_tips` (
  `tips_id` int(11) NOT NULL AUTO_INCREMENT,
  `tips_title` varchar(32) NOT NULL,
  `tips_content` text,
  `tips_open` int(11) DEFAULT NULL,
  PRIMARY KEY (`tips_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `gender` varchar(2) DEFAULT NULL,
  `pdate` datetime DEFAULT NULL,
  `lastdate` datetime DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `idcard` varchar(255) DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `roleid` int(11) DEFAULT NULL,
  PRIMARY KEY (`userid`) USING BTREE,
  KEY `fk_one` (`roleid`),
  CONSTRAINT `fk_one` FOREIGN KEY (`roleid`) REFERENCES `t_role` (`roleid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_vaccine
-- ----------------------------
DROP TABLE IF EXISTS `t_vaccine`;
CREATE TABLE `t_vaccine` (
  `id` int(11) NOT NULL,
  `vaccine_name` varchar(255) DEFAULT NULL,
  `vaccine_amount` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
