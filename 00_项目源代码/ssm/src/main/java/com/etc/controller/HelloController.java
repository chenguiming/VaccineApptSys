package com.etc.controller;

import com.etc.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {

    @Autowired
    private HelloService helloService;

    @RequestMapping("/hello")
    public String sayHello() {
        System.out.println("hello,springmvc");
        helloService.sayHello();
        return "index";
    }
}
