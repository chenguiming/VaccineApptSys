package com.etc.mapper;

import com.etc.entity.Goods;

public interface GoodsMapper {

    Goods queryById(int id);

    void update(Goods goods);

}
