package com.etc.service;

import com.etc.entity.Goods;
import com.etc.mapper.GoodsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HelloService {

    @Autowired
    private GoodsMapper goodsMapper;

    public void sayHello() {
        System.out.println("hello,spring");




        Goods goods2 = new Goods();
        goods2.setId(2);
        goods2.setName("b");
        goodsMapper.update(goods2);

        Goods goods1 = new Goods();
        goods1.setId(1);
        goods1.setName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        goodsMapper.update(goods1);
    }
}
