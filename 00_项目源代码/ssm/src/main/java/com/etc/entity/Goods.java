package com.etc.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Goods implements Serializable {
    private int id;
    private String name;
    private String brand;
    private double price;
    private int sales;
    private String color;
}
